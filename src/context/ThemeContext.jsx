import { createContext } from "react";

export const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee",
    title: "light",
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222",
    title: "dark",
  },
};

export const ThemeContext = createContext({
  theme: themes.light,
  toggleTheme: () => {},
});
