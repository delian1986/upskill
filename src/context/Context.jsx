import { Component } from "react";
import { ThemeContext, themes } from "./ThemeContext";
import { Toolbar } from "./Toolbar";

export default class Context extends Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: themes.light,
      toggleTheme: this.toggleTheme.bind(this),
    };
  }

  toggleTheme() {
    this.setState((state) => ({
      theme: state.theme === themes.light ? themes.dark : themes.light,
    }));
  }

  render() {
    return (
      <ThemeContext.Provider value={this.state}>
        <h4>Context example</h4>
        <Toolbar onClick={this.toggleTheme} />
      </ThemeContext.Provider>
    );
  }
}
