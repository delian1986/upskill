import { Button } from "../common/Button";
import { ThemeContext } from "./ThemeContext";

export const ThemedButton = () => {
  return (
    <ThemeContext.Consumer>
      {({ theme, toggleTheme }) => (
        <Button
          backgroundColor={theme.background}
          title={theme.title}
          onClick={toggleTheme}
        />
      )}
    </ThemeContext.Consumer>
  );
};
