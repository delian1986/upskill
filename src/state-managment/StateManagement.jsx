import { Counter } from "./Counter";
import { CountDisplay } from "./CountDisplay";
import { CountProvider } from "./CountContext";

export function StateManagement() {
  return (
    <>
      <h4>State Management Example</h4>
      <CountProvider>
        <CountDisplay />
        <Counter />
      </CountProvider>
    </>
  );
}
