import { Button } from "../common/Button";
import { useCountDispatch } from "./CountContext";

export function Counter() {
  const dispatch = useCountDispatch();

  const increment = () => {
    dispatch({ type: "INCREMENT" });
  };

  const decrement = () => {
    dispatch({ type: "DECREMENT" });
  };

  return (
    <>
      <Button onClick={increment} title="Increment" />
      <Button onClick={decrement} title="Decrement" />
    </>
  );
}
