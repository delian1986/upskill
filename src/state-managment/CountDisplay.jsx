import { useCountState } from "./CountContext";

export function CountDisplay() {
  const { count } = useCountState();
  return <div>The current count is: {count}</div>;
}
