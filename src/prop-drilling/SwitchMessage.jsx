export function SwitchMessage({ on }) {
  return <div>The button is {on ? "on" : "off"} </div>;
}
