import { Toggle } from "./Toggle";

export function PropDrilling() {
  return (
    <>
      <h4>Prop Drilling example</h4>
      <Toggle />
    </>
  );
}
