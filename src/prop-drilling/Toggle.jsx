import { useState } from "react";
import { Switch } from "./Switch";

export function Toggle() {
  const [on, setOn] = useState(false);

  const toggle = () => setOn((on) => !on);

  return <Switch on={on} onToggle={toggle} />;
}
