import { Button } from "../common/Button";
import { SwitchMessage } from "./SwitchMessage";

export function Switch({ on, onToggle }) {
  return (
    <>
      <SwitchMessage on={on} />
      <Button onClick={onToggle} title={"Toggle"} />
    </>
  );
}
