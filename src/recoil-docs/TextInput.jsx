import { useRecoilState } from "recoil";
import { textState } from "./atom/textState";

export function TextInput() {
  const [text, setText] = useRecoilState(textState);

  const onChange = (e) => {
    setText(e.target.value);
  };

  return (
    <>
      <input type="text" onChange={onChange} />
      <br />
      <p>{text}</p>
    </>
  );
}
