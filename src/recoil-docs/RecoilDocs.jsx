import { RecoilRoot } from "recoil";
import { CharacterCounter } from "./CharacterCounter";

export function RecoilDocs() {
  return (
    <>
      <h4>Recoil Official docs: Getting started</h4>
      <RecoilRoot>
        <CharacterCounter />
      </RecoilRoot>
    </>
  );
}
