import { TextInput } from "./TextInput";
import { CharacterCount } from "./CharacterCount";

export function CharacterCounter() {
  return (
    <>
      <TextInput />
      <CharacterCount />
    </>
  );
}
