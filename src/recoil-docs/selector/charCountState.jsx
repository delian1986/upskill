import { selector } from "recoil";
import { textState } from "../atom/textState";

export const charCountState = selector({
  key: "charStateCount",
  get: ({ get }) => {
    const text = get(textState);

    return text.length;
  },
});
