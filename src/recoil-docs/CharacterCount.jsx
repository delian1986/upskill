import { useRecoilValue } from "recoil";
import { charCountState } from "./selector/charCountState";

export function CharacterCount() {
  const count = useRecoilValue(charCountState);

  return <>Char Count: {count}</>;
}
