import {Link} from "react-router-dom";
import "./header.css";

export const Header = () => {
    return(
      <ul>
          <li>
              <Link to="/recoil-todo">
                  RecoilTodo
              </Link>
          </li>
          <li>
              <Link  to="/recoil-docs">
                  RecoilDocs
              </Link>
          </li>
          <li>
              <Link  to="/context">
                  Context
              </Link>
          </li>
          <li>
              <Link  to="/props-drilling">
                  PropDrilling
              </Link>
          </li>
          <li>
              <Link  to="/state-management">
                  StateManagement
              </Link>
          </li>
          <li>
              <Link  to="/context-api">
                  ContextApi
              </Link>
          </li>
      </ul>
    );
};