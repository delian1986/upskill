import { useRecoilValue } from "recoil";
import { TodoItemCreator } from "./TodoItemCreator";
import { TodoItem } from "./TodoItem";
import { filteredTodoListState } from "./selectors/filteredTodoListState";
import { TodoListFilters } from "./TodoListFilterers";
import { TodoListStats } from "./TodoListStats";

export const Todo = () => {
  const todoList = useRecoilValue(filteredTodoListState);
  return (
    <>
      <h4>Recoil Todo</h4>
      <TodoListFilters />
      <TodoItemCreator />
      <TodoListStats />

      {todoList.map((todoItem) => (
        <TodoItem key={todoItem.id} item={todoItem} />
      ))}
    </>
  );
};
