import {RecoilRoot} from "recoil";
import {Todo} from "./Todo";

export const RecoilTodo = () => {
    return (
        <RecoilRoot>
            <Todo />
        </RecoilRoot>
    );
};