import { useRecoilValue } from "recoil";
import { todoListStatsState } from "./selectors/todoListStatsState";

export function TodoListStats() {
  const {
    totalNum,
    totalCompleted,
    totalUncompleted,
    percentCompleted,
  } = useRecoilValue(todoListStatsState);

  const formattedPercentCompleted = Math.round(percentCompleted * 100);

  return (
    <ul>
      <li>Total items: {totalNum}</li>
      <li>Items completed: {totalCompleted}</li>
      <li>Items not completed: {totalUncompleted}</li>
      <li>Percent completed: {formattedPercentCompleted}</li>
    </ul>
  );
}
