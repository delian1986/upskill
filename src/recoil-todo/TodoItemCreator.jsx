import { useState } from "react";
import { useSetRecoilState } from "recoil";
import { todoListState } from "./atoms/todoListState";
import { Input } from "../common/Input";
import { Button } from "../common/Button";

export function TodoItemCreator() {
  const [inputValue, setInputValue] = useState("");
  const setTodoList = useSetRecoilState(todoListState);

  const addItem = () => {
    setTodoList((oldList) => [
      ...oldList,
      {
        id: getId(),
        text: inputValue,
        isComplete: false,
      },
    ]);
    setInputValue("");
  };

  const onChange = ({ target: { value } }) => {
    setInputValue(value);
  };

  return (
    <>
      <Input type="text" value={inputValue} onChange={onChange} />
      <Button onClick={addItem} title="Add" />
    </>
  );
}

let id = 0;
function getId() {
  return id++;
}
