import { useCountContext } from "./AppContext";
import { Button } from "../common/Button";

export function Count() {
  const { count, setCount } = useCountContext();
  return (
    <>
      <p>The count is: {count}</p>
      <Button onClick={() => setCount(count + 1)} title="Increment" />
    </>
  );
}
