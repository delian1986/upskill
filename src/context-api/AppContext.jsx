import { createContext, useContext, useMemo, useState } from "react";

const CountContext = createContext();
const MessageContext = createContext();

function useCountContext() {
  return useContext(CountContext);
}

function useMessageContext() {
  return useContext(MessageContext);
}

function MessageProvider(props) {
  const [message, useMessage] = useState("Hello from context");
  const value = { message, useMessage };

  return <MessageContext.Provider value={value} {...props} />;
}

function CountProvider(props) {
  const [count, setCount] = useState(0);

  const [message, setMessage] = useState("Hello from context");
  const value = useMemo(
    () => ({
      count,
      setCount,
      message,
      setMessage,
    }),
    [count, message]
  );

  return <CountContext.Provider value={value} {...props} />;
}

export { useCountContext, CountProvider, useMessageContext, MessageProvider };
