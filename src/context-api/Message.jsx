import { useMessageContext } from "./AppContext";

export const Message = () => {
  const { message } = useMessageContext();

  const getColor = () => Math.floor(Math.random() * 255);
  const style = {
    color: `rgb(${getColor()},${getColor()},${getColor()})`,
  };

  return <p style={style}>{message}</p>;
};
