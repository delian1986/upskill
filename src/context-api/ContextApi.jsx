import { Count } from "./Count";
import { CountProvider, MessageProvider } from "./AppContext";
import { Message } from "./Message";

export function ContextApi() {
  return (
    <>
      <h4>Context Api</h4>
      <h2>Re-renders! 😩</h2>
      <MessageProvider>
        <Message />
        <Message />
        <Message />
      </MessageProvider>
      <CountProvider>
        <Count />
      </CountProvider>
    </>
  );
}
