export const Button = (props) => {
  console.log(props);
  return (
    <button
      style={{ backgroundColor: props.backgroundColor }}
      onClick={props.onClick}
    >
      {props.title}
    </button>
  );
};
