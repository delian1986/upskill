import { hot } from "react-hot-loader/root";
import { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Context from "./context/Context";
import { PropDrilling } from "./prop-drilling/PropDrilling";
import { StateManagement } from "./state-managment/StateManagement";
import { ContextApi } from "./context-api/ContextApi";
import { RecoilDocs } from "./recoil-docs/RecoilDocs";
import {RecoilTodo} from "./recoil-todo/RecoilTodo";
import {Header} from "./header/Header";

class App extends Component {
  render() {
    return (
      <>
          <Router>
            <Header/>
            <Switch>
              <Route path={"/recoil-todo"} component={RecoilTodo}/>
              <Route path={"/recoil-docs"} component={RecoilDocs}/>
              <Route path={"/context"} component={Context}/>
              <Route path={"/props-drilling"} component={PropDrilling}/>
              <Route path={"/state-management"} component={StateManagement}/>
              <Route path={"/context-api"} component={ContextApi}/>
            </Switch>
          </Router>
      </>
    );
  }
}

export default hot(App);
